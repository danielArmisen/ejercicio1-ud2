package sumandos;

import java.util.Scanner;

public class Sumando {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		
		int n1,n2;
		
		System.out.println("Escribe un numero");
		n1 = in.nextInt();
		System.out.println("Escribe otro numero");
		n2 = in.nextInt();
		
		System.out.println("La suma entre " + n1 + " y " + n2 + " es " + (n1+n2));
		
	}

}
